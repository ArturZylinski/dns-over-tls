import ssl

import asyncio

from config import (
    HOST, PORT, DNS_RESOLVER_IP, DNS_RESOLVER_PORT,
    SSL_CERTFILE_PATH, SSL_KEYFILE_PATH
)


ssl_context = ssl.create_default_context(ssl.Purpose.CLIENT_AUTH)
ssl_context.load_cert_chain(SSL_CERTFILE_PATH, SSL_KEYFILE_PATH)


async def pipe(reader, writer):
    try:
        while not reader.at_eof():
            data = await reader.read(4096)
            # print(data.decode())
            writer.write(data)
    finally:
        writer.close()


async def handler(reader, writer):

    # TODO: validation
    # TODO: accept only valid DNS lookups queries, ignore the rest

    try:
        remote_reader, remote_writer = await asyncio.open_connection(
            DNS_RESOLVER_IP, DNS_RESOLVER_PORT, ssl=ssl_context)
        pipe1 = pipe(reader, remote_writer)
        pipe2 = pipe(remote_reader, writer)
        await asyncio.gather(pipe1, pipe2)
    finally:
        writer.close()


loop = asyncio.get_event_loop()
coro = asyncio.start_server(handler, HOST, PORT, loop=loop)
server = loop.run_until_complete(coro)

# Serve requests until Ctrl+C is pressed
print('Serving on {}'.format(server.sockets[0].getsockname()))
try:
    loop.run_forever()
except KeyboardInterrupt:
    pass

# Close the server
server.close()
loop.run_until_complete(server.wait_closed())
loop.close()
