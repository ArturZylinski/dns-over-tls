from os import getenv


HOST = '0.0.0.0'  # default Docker IP
PORT = 53

# Cloudflare
DNS_RESOLVER_IP = '1.1.1.1'
DNS_RESOLVER_PORT = 853

SSL_CERTFILE_PATH = '/etc/ssl/certs/selfsigned.cert'
SSL_KEYFILE_PATH = '/etc/ssl/certs/selfsigned.key'
