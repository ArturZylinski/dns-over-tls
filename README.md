# DNS over TLS

Generate certs

```bash
openssl req -x509 -newkey rsa:2048 -keyout selfsigned.key -nodes -out selfsigned.cert -sha256 -days 1000
```

## Run

```bash
# build docker image
docker build -t dot .

# run
docker run --rm -p 53:53 dot
```

## Test

Use [wireshark](https://www.wireshark.org) to see how the Application Data is sent to Cloudflare (filter: `ip.dst == 1.1.1.1`).

```bash
dig -d @127.0.0.1 +tcp www.onet.pl
```

Use [kdig](https://www.knot-dns.cz/) for more debug info.

Full test explanation and example: https://www.youtube.com/watch?v=Z-IfN5LhCXE


## Task Questions

> What are the security concerns for this kind of service?

* The data between an app and this dns-proxy is still internaly readable.
* Atm, all TPC data has been forwarder into Cloudflare. There should be DNS Packet Structure validation.
* SSL keys should not be inside docker image. Better stored in Secret Management tool (like [vault](https://www.vaultproject.io/)).

> Considering a microservice architecture; how would you see this the dns to dns-over-tls proxy used?

* For unix system, the `/etc/resolv.conf` file could be pointing only to this proxy. (although, it doesn't stop app clients to hardcode their own custom resolver, so for example to use `1.1.1.1` directly).

> What other improvements do you think would be interesting to add to the project?

* Write same functionality in Go/C++.
* Measure/compare latency impact in production.
