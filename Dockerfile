FROM python:3.7.0-alpine

# copy prebuild certs inside an image
COPY selfsigned.cert selfsigned.key /etc/ssl/certs/

COPY requirements.txt /tmp/
RUN pip install --no-cache-dir -r /tmp/requirements.txt

RUN mkdir -p /src/usr/app
COPY src /src/usr/app

WORKDIR /src/usr/app

EXPOSE 53

CMD ["python", "main.py"]